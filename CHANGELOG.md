# Changelog

Changelog for tmux-dpm. All notable changes to this project will be documented in this file.

## 2024-09-02

- License changed from MIT to GNU Affero General Public License v3.0 (AGPLv3).
- Updated `README.md` and `LICENSE` files to reflect the new license and other related changes.

## 2024-07-20

- Updated `LICENSE` file to provide updated author information.
- Updated `README.md` file with an updated program description and license information.

## 2022-08-20

- Added `CHANGELOG.md` file and update `README.md` accordingly

## 2021-02-02

- Project release
